" Prevent from loading the plugin again.
if (exists('g:loaded_lsp_ale') && g:loaded_lsp_ale)
  finish
endif
let g:loaded_lsp_ale = 1

" Configure nvim_lsp_ale.
lua require'nvim_lsp_ale'.setup()
