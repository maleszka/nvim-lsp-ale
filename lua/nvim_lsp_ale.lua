local sev_map = {
  [vim.lsp.protocol.DiagnosticSeverity.Error] = 1,
  [vim.lsp.protocol.DiagnosticSeverity.Warning] = 2,
  [vim.lsp.protocol.DiagnosticSeverity.Information] = 3,
  [vim.lsp.protocol.DiagnosticSeverity.Hint] = 4,
}

-- Process diagnostics and send to ALE.
local function send_to_ale(bufnr, diagnostics)
  -- Prevent from sending diagnostics to a not existing buffer.
  if not bufnr then
    return
  end

  -- Tell ALE that nvim-lsp has already started checking.
  vim.fn['ale#other_source#StartChecking'](bufnr, 'nvim-lsp')

  -- If there is no diagnostics.
  if not diagnostics or vim.tbl_isempty(diagnostics) then
    vim.fn['ale#other_source#ShowResults'](bufnr, 'nvim-lsp', {})
    return
  end

  -- There will be stored converted diagnostics.
  local messages = {}
  for _, event in ipairs(diagnostics) do
    local msg = {}
    msg.text = '[' .. event.source .. '] ' .. event.message
    msg.lnum = event.range.start.line + 1
    msg.end_lnum = event.range['end'].line + 1
    msg.col = event.range.start.character + 1
    msg.end_col = event.range['end'].character
    msg.bufnr = bufnr
    msg['type'] = sev_map[event.severity]
    msg.code = event.code
    table.insert(messages, msg)
  end

  -- Submit messages to ALE.
  vim.fn['ale#other_source#ShowResults'](bufnr, 'nvim-lsp', messages)
end

-- This function handles diagnostics requested by nvim-lsp
local function publish_handler(_, _, params, client_id, _, config)
  -- Get bufnr
  local bufnr = vim.uri_to_bufnr(params.uri)
  if not bufnr then
    -- Because nvim-lsp runs asynchronously, checked buffer can be deleted
    -- during language server's process.
    return
  end

  -- Finally, send to ALE.
  send_to_ale(bufnr, params.diagnostics)
end

local function setup()
  -- Automatically configure ALE not to use LSP.
  if vim.fn.get(vim.g, 'lsp_ale_auto_conf', true) then
    -- Disable LSP in ALE.
    vim.g.ale_disable_lsp = 1
  end

  -- Set nvim-lsp diagnostics publishing handler.
  vim.lsp.handlers["textDocument/publishDiagnostics"] = publish_handler
end

return {
  setup = setup,
}
